# vp30

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve:electron
```

### Compiles and minifies for production
```
yarn run build:electron
```
