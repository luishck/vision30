import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// Plugins
// import './registerServiceWorker'
import './plugins/element.js'

// Initialize Users
import './plugins/user-initializer.js'

import ionicon from '@/components/ui/ionicon.vue'

Vue.component('ionicon', ionicon)

Vue.config.productionTip = false

// MomentJS
import moment from 'moment'
import 'moment/locale/es'

Vue.use(require('vue-moment'), moment)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
