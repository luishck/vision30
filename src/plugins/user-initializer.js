import { User } from "../database/models"
import { hashText } from '../lib/crypto'

User.findOrCreate({
    where: { username: 'admin' },
    defaults: {
        firstname: 'admin',
        passwordDigest: hashText('vision30'),
        phone: '27152506'
    }
})
