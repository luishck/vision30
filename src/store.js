import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentUser: {},
    navbarActions: [],
    adReproductions: [],
    adInterval: undefined
  },
  mutations: {
    setCurrentUser(state, user) {
      state.currentUser = user
    },

    setNavbarActions(state, actions) {
      state.navbarActions = actions
    },

    setAdReproduction(state, reg) {
      state.adReproductions.unshift(reg)
    },

    setAdInterval(state, interval) {
      state.adInterval = interval
    },

    stopAdInterval(state) {
      clearInterval(this.state.adInterval)
      state.adInterval = undefined
    }
  },
  actions: {
    removeNavbarActions(context) {
      context.commit('setNavbarActions', [])
    },
    regiterReproduction(context) {
      context.commit('setAdReproduction', new Date().toLocaleString())
    }
  },
  getters: {
    isLogged: state => {
      if (state.currentUser.id !== undefined) {
        return true
      } else {
        return false
      }
    }
  },
  plugins: [
    createPersistedState({
      reducer: persistedState => {
        const stateFilter = Object.assign({}, persistedState)
        const blackList = ['currentUser']

        blackList.forEach(item => {
          delete stateFilter[item]
        })

        return stateFilter
      }
    })
  ]
})
