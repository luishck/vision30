import Sequelize from 'sequelize'
import path from 'path'
import { remote } from 'electron'

const db = new Sequelize('vp30', null, null, {
  host: 'localhost',
  dialect: 'sqlite',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  storage: path.join(remote.app.getPath('userData'), 'vp30')
})

export default db
