import db from '../index'
import Sequelize from 'sequelize'

const User = db.define(
  'user',
  {
    firstname: Sequelize.STRING,
    lastname: Sequelize.STRING,
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    passwordDigest: {
      type: Sequelize.STRING,
      allowNull: false
    },
    phone: Sequelize.STRING,
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    }
  },
  {
    timestamps: true
  }
)

export default User
