import db from '../'
import User from './User.model'
import Sponsor from './Sponsor.model'
import Tier from './Tier.model'
import Ad from './Ad.model'

/**
 * RELATIONSHIPS
 */

// Sponsor
Sponsor.hasMany(Ad)

// Ad
Ad.belongsTo(Sponsor)
Ad.hasOne(Tier)

db.sync()

export { Sponsor, Tier, User, Ad }
