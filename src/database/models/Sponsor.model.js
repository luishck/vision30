import db from '../index'
import Sequelize from 'sequelize'

const Sponsor = db.define(
  'sponsor',
  {
    name: Sequelize.STRING,
    address: Sequelize.STRING,
    phone: Sequelize.STRING,
    facebook: Sequelize.STRING,
    notes: Sequelize.TEXT,
    other: Sequelize.TEXT,
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    }
  },
  {
    timestamps: true
  }
)

export default Sponsor
