import db from '../index'
import Sequelize from 'sequelize'

const Ad = db.define(
  'ad',
  {
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    start_date: Sequelize.DATE,
    video_name: Sequelize.STRING,
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    }
  },
  {
    timestamps: true
  }
)

export default Ad
