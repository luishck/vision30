import db from '../index'
import Sequelize from 'sequelize'

const Tier = db.define(
  'tier',
  {
    name: Sequelize.STRING,
    description: Sequelize.TEXT,
    price: Sequelize.DECIMAL,
    mentions: Sequelize.INTEGER,
    hours: Sequelize.STRING
  },
  {
    timestamps: true
  }
)

export default Tier
