import { remote } from 'electron'
import path from 'path'
import fs from 'fs'

export default class FileManager {
  /**
   * Return the absolute path for the new file
   * @param {String} rootPath Default path for file
   * @param {String} fileName Name of the file
   */
  setFilePath(rootPath, fileName) {
    return path.join(remote.app.getPath(rootPath), fileName)
  }

  /**
   * Write a new file in given path
   * @param {String} filePath Destination of the new file
   * @param {String} content UTF-8 String format
   */
  writeFile(filePath, content) {
    return new Promise((resolve, reject) => {
      fs.writeFile(filePath, content, 'utf-8', err => {
        if (err) {
          reject(false)
        } else {
          resolve(true)
        }
      })
    })
  }

  /**
   * Read a file from a given path
   * @param {String} filePath Absolute path for the filw
   * @returns {Promise} Promise of a file or an error
   */
  readFile(filePath) {
    return new Promise((resolve, reject) => {
      // Read the file
      fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  }

  MistypeException(type) {
    return `Expected type must be ${type}`
  }
}
