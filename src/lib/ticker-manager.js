import { remote } from 'electron'
import path from 'path'
import fs from 'fs'

function setPath(fileName) {
  return path.join(remote.app.getPath('documents'), 'vMixStorage/', fileName)
}

/**
 * Write a file in default path
 * @param {String} fileName
 * @param {String} content
 * @returns {Boolean} Returns true if the file is writed
 */
export function write(fileName, content) {
  const filePath = setPath(fileName)
  fs.writeFile(filePath, content, 'utf-8', err => {
    if (err) {
      return false
      console.err("Can't save the file")
    } else {
      return true
    }
  })
}

/**
 * Read a file by name
 * @param {String} fileName
 * @returns {Promise} File utf-8 or an exception
 */
export function read(fileName) {
  const filePath = setPath(fileName)
  // Returns a new promise
  return new Promise((resolve, reject) => {
    // Read file from path
    fs.readFile(filePath, 'utf-8', (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}
