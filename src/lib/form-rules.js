const checkNumber = (rule, value, callback) => {
  const n = Number(value)
  if (Number.isNaN(n)) {
    callback(new Error('Sólo se permiten números'))
  } else {
    if (value < 0) {
      callback(new Error('Debe ser mayor que 0 (cero)'))
    } else {
      callback()
    }
  }
}
export const formRules = {
  required: { required: true, message: 'Campo requerido', trigger: 'blur' },
  requiredSelect: {
    required: true,
    message: 'Selecciona una opción',
    trigger: 'change'
  },
  number: { validator: checkNumber, trigger: 'blur' }
}

