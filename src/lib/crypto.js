import bcrypt from 'bcrypt'
const saltRounds = 11

/**
 * Hash a plain text password
 * @param {String} plainText Plain text password
 */
export function hashText(plainText) {
  let salt = bcrypt.genSaltSync(saltRounds)
  return bcrypt.hashSync(plainText, salt)
}

/**
 * Try to compare hashed password with stored in db
 * @param {String} plainText Plain text password
 * @param {String} hash Hashed password
 * @returns Boolean
 */
export function compareHashes(plainText, hash) {
  return bcrypt.compareSync(plainText, hash) 
}
