import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
// Import pages
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Sponsors from './views/Sponsors.vue'
import Tiers from './views/Tiers.vue'
import Tier from './views/Tier.vue'
import Ads from './views/Ads.vue'
import Ticker from './views/Ticker.vue'
import Scheduler from './views/Scheduler.vue'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        title: 'Inicio'
      }
    },
    {
      path: '/',
      name: 'login',
      component: Login,
      meta: {
        public: true,
        title: 'Inicio de Sesión'
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue'),
      meta: {
        title: 'Acerca'
      }
    },
    {
      path: '/sponsors',
      name: 'sponsors',
      component: Sponsors,
      meta: {
        title: 'Patrocinadores'
      }
    },
    {
      path: '/sponsor',
      name: 'sponsor',
      component: () => import('./views/Sponsor.vue'),
      meta: {
        title: 'Patrocinador',
        canBack: true
      }
    },
    {
      path: '/tiers',
      name: 'tiers',
      component: Tiers,
      meta: {
        title: 'Planes Publicitarios',
        canBack: true
      }
    },
    {
      path: '/tier',
      name: 'tier',
      component: Tier,
      meta: {
        title: 'Plan Publicitario',
        canBack: true
      }
    },
    {
      path: '/ads',
      name: 'ads',
      component: Ads,
      meta: {
        title: 'Comerciales'
      }
    },
    {
      path: '/ticker',
      name: 'ticker',
      component: Ticker,
      meta: {
        title: 'Cinta'
      }
    },
    {
      path:'/scheduler',
      mame: 'scheduler',
      component: Scheduler,
      meta: {
        title: 'Programador Automático'
      }
    },
    {
      path:'/playlists',
      mame: 'playlists',
      component: () => import('./views/Playlists.vue'),
      meta: {
        title: 'Listas de Reproduccion'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.public)) {
    if (store.getters.isLogged) {
      next({ name: 'home' })
    } else {
      next()
    }
  } else {
    if (store.getters.isLogged) {
      next()
    } else {
      next({ name: 'login' })
    }
  }
})

export default router
